import html from './index.html';
import './style.css';

document.body.innerHTML = html;

const navButton = document.querySelector('.nav__button');
const menuImage = document.querySelector('.nav__menu-image');
const closeImage = document.querySelector('.nav__close-image');
const navList = document.querySelector('.nav__list');
const nav = document.querySelector('.nav');

navButton.addEventListener('click', () => {
  navList.classList.toggle('nav__list--visible');
  nav.classList.toggle('nav--visible');
  menuImage.classList.toggle('image--invisible');
  closeImage.classList.toggle('image--invisible');
});

window.addEventListener('scroll', () => {
  const parallaxElements = Array.prototype.slice.call(document.getElementsByClassName('parallax'));
  parallaxElements.forEach((element) => {
    const position = element.getBoundingClientRect();
    if (position.top - window.innerHeight < 0) element.style.top = -window.scrollY * 0.5 + 'px';
  });
});
